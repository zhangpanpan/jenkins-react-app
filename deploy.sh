#!/usr/bin/env bash

cd $PROJ_PATH/jenkins-react-app
rm -rf node_modules/
npm install
npm run start
