import React, {Component} from 'react';
import './index.css';

class Index extends Component {
    render() {
        return (
            <div className='container'>
                <p className='title'>
                    Jenkins - React 打包测试流水线
                </p>
                <p>
                    author: panpanzhang
                </p>
            </div>
        );
    }
}

export default Index;
